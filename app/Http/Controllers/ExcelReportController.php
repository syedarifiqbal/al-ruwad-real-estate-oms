<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaleOfferStore;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class ExcelReportController extends Controller
{
    private $primaryColor = '0073ca';
    private $offer;
    private $tableRow = 12;

    public function index(Request $request, $first = 'first')
    {
        $this->offer = session('offer');
        if(!$this->offer){
            return redirect('/');
        }

        try{
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $this->setUpSheet($sheet);
            $this->addLogo($sheet);
            $this->addDate($sheet);
            $sheet->setCellValue("B7", "Dear " . $this->offer['customerName'] . ',')->getStyle("B7")->getFont()->setBold(true);
            $sheet->setCellValue("B9", $this->offer['welcomeNote']);
            $this->renderProjectTable($sheet);
            $this->renderProjectPaymentTable($sheet);

            $sheet->setCellValue("B$this->tableRow", "*Please note that the availabilities are subject to change")
                ->getStyle("B$this->tableRow")
                ->getFont()
                ->setSize(14)
                ->setBold(true)
                ->setUnderline(true)
                ->setItalic(true);
            $this->tableRow++;
            $this->tableRow++;

            $sheet->setCellValue("B$this->tableRow", "Sincerely,")
                ->getStyle("B$this->tableRow")
                ->getFont()
                ->setSize(13)
                ->setBold(true);
            $this->tableRow++;
            $this->tableRow++;

            $sheet->setCellValue("B$this->tableRow", $this->offer['brokerName']);
            $this->tableRow++;

            $sheet->setCellValue("B$this->tableRow", $this->offer['brokerDesignation']);
            $this->tableRow++;

            $sheet->setCellValue("B$this->tableRow", "BRN: ".$this->offer['brokerRegistrationNumber'])
                ->getStyle("B$this->tableRow");
            $this->tableRow++;

            $writer = new Xlsx($spreadsheet);
            /*$writer->save('project.xlsx');*/

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'. time() .'-sales-offer.xlsx"');
            $writer->save("php://output");
            // return $this->offer;
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    private function addLogo(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet)
    {
        try{
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Logo');
            $drawing->setDescription('Logo');
            $drawing->setPath(realpath(public_path("img/logo.png") ));
            $drawing->setHeight(75);
            $drawing->setCoordinates("B2");
            $drawing->setWorksheet($sheet);
        }catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    private function addDot(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet, $cell)
    {
        try{
            $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $drawing->setName('Dot');
            $drawing->setDescription('Dot');
            $drawing->setPath(realpath(public_path("img/dot.png") ));
            $drawing->setOffsetY(7);
            $drawing->setHeight(8);
            $drawing->setCoordinates($cell);
            $drawing->setWorksheet($sheet);
        }catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    private function addDate(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet)
    {
        try{
            $sheet->setCellValue('G2', 'Date:')
                ->getStyle('G2')->getFont()->setBold(true);

            $sheet->getStyle('G2')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);

            $sheet->setCellValue('H2', date('d-M-Y'))
                ->getStyle('H2')->getFont()->setBold(true);

            $sheet->setCellValue('B5', "SALES OFFER")
                ->getStyle('B5')
                ->getFont()
                ->setBold(true)
                ->setSize(36)
                ->getColor()
                ->setRGB($this->primaryColor);

            $sheet->getStyle('B5')
                ->getAlignment()
                ->setVertical(Alignment::VERTICAL_CENTER)
                ->setHorizontal(Alignment::HORIZONTAL_CENTER);

            $sheet->getRowDimension("B5")->setRowHeight(70);
            $sheet->mergeCells("B5:H5");

        }catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    private function setUpSheet(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet)
    {
        $sheet->setShowGridlines(false);
        $sheet->getDefaultRowDimension()->setRowHeight(25);
        $sheet->getColumnDimension('A')->setWidth(2);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(25);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(21);
        $sheet->getColumnDimension('F')->setWidth(20);
        $sheet->getColumnDimension('G')->setWidth(20);
        $sheet->getColumnDimension('H')->setWidth(15);
        $sheet->getRowDimension(12)->setRowHeight(45);

        $styleArray = array(
            'font'  => array(
                'color' => array('rgb' => '717271'),
            ));

        $sheet->getStyle('A:I')->applyFromArray($styleArray);

        $sheet->getRowDimension(9)->setRowHeight(50);
        $sheet->getStyle('B9')->getAlignment()->setWrapText(true)->setVertical(Alignment::VERTICAL_CENTER);
        $sheet->mergeCells("B9:H9");
    }

    private function renderProjectTable(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet)
    {
        try{
            $cols = [
                "Project / Location",
                "Plot #",
                "Plot Size (SQ. FT)",
                "Build Up Area (SQ. FT)",
                "Price/SQ. FT (AED)",
                "Total Price (AED)",
                "Plot Use",
            ];
            $colName = "B";
            $tableStartRow = $this->tableRow;
            foreach($cols as $col){
                $sheet->setCellValue($colName . $this->tableRow, $col)
                    ->getStyle($colName . $this->tableRow)->getFont()->getColor()->setARGB(Color::COLOR_WHITE);
                $colName++;
            }

            $sheet->getStyle("B12:H12")->applyFromArray($this->getTableHeaderStyle());
            $numberFormats = [2, 4, 5];
            // Make Table from projects data
            $this->tableRow++;
            foreach($this->offer['projects'] as $index =>  $project)
            {
                $colName = "B";
                $cols = array_keys($project);
                foreach($cols as $colIndex => $col)
                {
                    // skip column if it is not or payment plan for particular project
                    if(in_array($col, ['paymentPlan', 'notes'])) continue;
                    // set cell text
                    $sheet->setCellValue($colName . $this->tableRow, $project[$col])
                        // set cell formatting
                        ->getStyle($colName . $this->tableRow)->applyFromArray($this->getTableRowCellFormat($index));

                    if(in_array($colIndex, $numberFormats))
                    {
                        $sheet->getStyle($colName . $this->tableRow)
                            ->getNumberFormat()
                            // ->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD, "#,##0.00");
                            ->setFormatCode("#,##0_-");

                        $sheet->getStyle($colName . $this->tableRow)
                            ->getAlignment()
                            // ->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD, "#,##0.00");
                            ->setHorizontal(Alignment::HORIZONTAL_RIGHT);
                    }
                    // increment column name
                    $colName++;
                }
                // increment row number
                $this->tableRow++;
            }
            // Set Center align for table rows
            $sheet->getStyle("B{$tableStartRow}:H{$this->tableRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }

    private function renderProjectPaymentTable(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet)
    {
        $this->tableRow++;

        try{
            foreach($this->offer['projects'] as $projectIndex => $project)
            {

                $paymentPlans = $project['paymentPlan'];
                $sheet->setCellValue("B" . $this->tableRow, $paymentPlans['noYears'] . " Years Payment Plan for " . $project['projectLocation'])
                    ->getStyle("B$this->tableRow")
                    ->getFont()->setBold(true)->setSize(14)->getColor()->setRGB($this->primaryColor);

                $this->tableRow+=2;
                $tableStartRow = $this->tableRow;

                $cols = ["#", "MILESTONE", "%", "DATE", "AMOUNT (AED)"];
                $colName = "B";
                $sheet->setCellValue($colName . $this->tableRow, "#")
                    ->getStyle($colName . $this->tableRow)->getFont()->getColor()->setARGB(Color::COLOR_WHITE);
                $colName++;

                $sheet->setCellValue($colName . $this->tableRow, "MILESTONE")
                    ->getStyle($colName . $this->tableRow)->getFont()->getColor()->setARGB(Color::COLOR_WHITE);
                $colName++; $colName++; $colName++;

                $sheet->setCellValue($colName . $this->tableRow, "%")
                    ->getStyle($colName . $this->tableRow)->getFont()->getColor()->setARGB(Color::COLOR_WHITE);
                $colName++;

                $sheet->setCellValue($colName . $this->tableRow, "DATE")
                    ->getStyle($colName . $this->tableRow)->getFont()->getColor()->setARGB(Color::COLOR_WHITE);
                $colName++;

                $sheet->setCellValue($colName . $this->tableRow, "AMOUNT (AED)")
                    ->getStyle($colName . $this->tableRow)->getFont()->getColor()->setARGB(Color::COLOR_WHITE);

                $sheet->getStyle("B$this->tableRow:H$this->tableRow")->applyFromArray($this->getTableHeaderStyle());

                $sheet->getRowDimension($this->tableRow)->setRowHeight(45);

                $this->tableRow++;
                foreach($paymentPlans['milestones'] as $milestoneIndex => $milestone)
                {
                    $colName = "B";
                    $sheet->setCellValue($colName . $this->tableRow, $milestoneIndex+1)
                        ->getStyle($colName . $this->tableRow)->applyFromArray($this->getTableRowCellFormat($milestoneIndex));

                    $milestone['amount'] = ($project['totalPrice'] / 100) * $milestone['percentage'];
                    $cols = array_keys($milestone);
                    $colName++;
                    foreach ($cols as $col)
                    {
                        // merging cells if column is milestone for better matching width with project table.
                        if($col === 'milestone'){
                            $sheet->mergeCells("C$this->tableRow:E$this->tableRow");
                            $sheet->setCellValue("C" . $this->tableRow, $milestone[$col]);
                            $sheet->getStyle("C$this->tableRow:E$this->tableRow")->applyFromArray($this->getTableRowCellFormat($milestoneIndex));
                            $colName = "F";
                        }else{
                            $sheet->setCellValue($colName . $this->tableRow, $milestone[$col])
                                ->getStyle($colName . $this->tableRow)->applyFromArray($this->getTableRowCellFormat($milestoneIndex));

                            if($col == 'amount')
                            {
                                $sheet->getStyle($colName . $this->tableRow)
                                    ->getNumberFormat()
                                    // ->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD, "#,##0.00");
//                                    ->setFormatCode("[\$AED] #,##0.00_-")
                                    ->setFormatCode("#,##0");

                                $sheet->getStyle($colName . $this->tableRow)
                                    ->getAlignment()
                                    ->setHorizontal(Alignment::HORIZONTAL_RIGHT);
                            }

                            if($col == 'date')
                            {
                                $sheet->setCellValue($colName . $this->tableRow, $milestone[$col]? date('M d, Y', strtotime($milestone[$col])): 'N/A')
                                        ->getStyle($colName . $this->tableRow)->applyFromArray($this->getTableRowCellFormat($milestoneIndex));

                                $sheet->getStyle($colName . $this->tableRow)
                                    ->getNumberFormat()
                                    // ->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD, "#,##0.00");
                                    ->setFormatCode(NumberFormat::FORMAT_DATE_DDMMYYYY);

                                $sheet->getStyle($colName . $this->tableRow)
                                    ->getAlignment()
                                    ->setHorizontal(Alignment::HORIZONTAL_RIGHT);
                            }

                            $colName++;
                        }
                    }
                    $this->tableRow++;
                }
                $sheet->getStyle("B{$tableStartRow}:H{$this->tableRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                $notes = $project['notes'];

                if(!empty($notes))
                {
                    $this->tableRow++;$this->tableRow++;
                    $colName = "B";
                    $sheet->setCellValue($colName . $this->tableRow, "Notes:")
                        ->getStyle($colName . $this->tableRow)->getFont()->setBold(true)
                        ->setSize(14)->getColor()->setRGB($this->primaryColor);
                    $this->tableRow++;
                }

                foreach($notes as $note)
                {
                    $colName = "B";
                    $sheet->setCellValue($colName . $this->tableRow, "    ".$note['title']);
                    $this->addDot($sheet, $colName . $this->tableRow);
                    $this->tableRow++;
                }

                $this->tableRow+=2;
            }

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }

    private function getTableHeaderStyle()
    {
        return [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => [
                    'argb' => $this->primaryColor,
                ],
            ],
        ];
    }

    private function getTableRowCellFormat($index)
    {
        $style = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [ 'argb' => '00cccccc' ]
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [ 'argb' => '00cccccc' ]
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [ 'argb' => '00cccccc' ]
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => [ 'argb' => '00cccccc' ]
                ],
            ],
        ];
        if($index%2)
        {
            $style['fill'] = [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'color' => [
                    'argb' => '00eeeeee',
                ],
            ];
        }
        return $style;
    }
}
