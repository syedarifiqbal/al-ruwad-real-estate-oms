<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaleOfferStore;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use PhpOffice\PhpWord\Style\ListItem;


class WordReportController extends Controller
{
    private $primaryColor = '0073ca';
    private $textColor = '717271';
    private $offer;
    private $form;

    public function index(Request $request, $first = 'first')
    {
        // the session is stored as offer and offer2 for second form
        // also i used same controller and methods to generate word file for both form by identifying via $first variable.
        $this->offer = session(($first === 'first') ? 'offer' : 'offer2');

        $this->form = $first;

        if (!$this->offer) {
            return redirect('/');
        }

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        // Adding an empty Section to the document...
        $section = $phpWord->addSection();

        $section->getStyle()->setMarginTop(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(.5));
        $section->getStyle()->setMarginLeft(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(.5));
        $section->getStyle()->setMarginRight(\PhpOffice\PhpWord\Shared\Converter::inchToTwip(.5));

        $this->setTopImage($section);

        // Adding Text element to the Section having font styled by default...
        $section->addText("Sales Offer", ["name" => "Tahoma", "size" => 24, 'bold' => true, 'color' => $this->primaryColor], ['align' => 'center']);

        $section->addText("Dear " . htmlspecialchars($this->offer['customerName']) . ',', ['bold' => true, 'size' => 12, 'color' => $this->textColor]);
        $section->addText("");

        $welcomeNote = preg_replace("/\r\n|\r|\n/", '<w:br/>', htmlspecialchars($this->offer['welcomeNote']));

        $section->addText($welcomeNote, $this->getParagraphStyle());

        $section->addText('', $this->getParagraphStyle());

        // Create new Table for projects

        // project table
        $this->renderProjectTable($phpWord, $section);
        // Create new Table for Payment Plan table
        $this->renderPaymentPlanTable($phpWord, $section);

        $section->addText("\n");
        $section->addText("*Please note that the availabilities are subject to change", ['bold' => true, 'italic' => true, 'underline' => 'single', 'color' => $this->textColor]);
        /*$section->addLine(['weight' => 1, 'width' => 1000, 'height' => 0, 'color' => 635552, 'margin' => 100]);*/
        $section->addText("\n");
        $section->addText("Sincerely,", ['bold' => true, 'color' => $this->textColor]);
        $section->addText("\n");
        $section->addText("\n");
        $section->addText(htmlspecialchars($this->offer['brokerName']), ['color' => $this->textColor]);
        $section->addText(htmlspecialchars($this->offer['brokerDesignation']), ['color' => $this->textColor]);

        if($this->form === 'first')
            $section->addText("BRN: " . htmlspecialchars($this->offer['brokerRegistrationNumber']), ['color' => $this->textColor]);

        $this->setFooter($section);

        $file = time() . '_sales_offer.docx';
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $file . '"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $xmlWriter->save("php://output");

        // return $this->offer;
    }

    private function getParagraphStyle($fontSize = 10)
    {
        return ["name" => "Tahoma", "size" => $fontSize, 'color' => $this->textColor];
    }

    private function renderProjectTable($phpWord, $section)
    {
        // start project table
        $tableStyle = array('borderColor' => '006699', 'borderSize' => 6, 'color' => 'ffffff', 'cellMargin' => 100, 'bold' => true);

        $firstRowStyle = array('bgColor' => $this->primaryColor, 'color' => 'ffffff', 'bold' => true, 'width' => 500);

        $phpWord->addTableStyle('projectTable', $tableStyle, $firstRowStyle);

        // Add header
        $projectTable = $section->addTable('projectTable');

        $this->displayProjectTableHeader($projectTable);

        // Append Rows
        foreach ($this->offer['projects'] as $projectIndex => $project) {
            $row    = $projectTable->addRow(12);
            $cStyle = $projectIndex % 2 ? ['bgColor' => 'eeeeee'] : [];

            // Add individual Cell like | location | plot no | and so on...
            foreach (array_keys($project) as $index => $col) {
                if (is_array($project[$col])) continue;

                $text = $project[$col];
                if (in_array($index, [2, 4, 5]) && is_numeric($project[$col])) {
                    $text = number_format($project[$col]);
                }
                $cell = $row->addCell(null, $cStyle);
                $cell->addText(htmlspecialchars($text), ['color' => $this->textColor], array('align' => 'center'));
            }
        }
    }

    private function renderPaymentPlanTable($phpWord, Section $section)
    {
        $tableStyle = new \PhpOffice\PhpWord\Style\Table;
        $tableStyle->setBgColor('006699');
        $tableStyle->setBorderColor('006699');
        $tableStyle->setBorderSize(6);
        $tableStyle->setUnit(\PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT);
        $tableStyle->setWidth(100 * 50);
        $tableStyle->setCellMargin(100);

        $firstRowStyle = array('bgColor' => $this->primaryColor, 'color' => 'ffffff', 'bold' => true);
        $phpWord->addTableStyle('paymentsTable', $tableStyle, $firstRowStyle);

        // Append Rows
        foreach ($this->offer['projects'] as $projectIndex => $project) {
            $paymentPlans = $project['paymentPlan'];

            $section->addText("\n");
            $section->addText(htmlspecialchars($paymentPlans['noYears']) . " " . str_plural("years", htmlspecialchars($paymentPlans['noYears'])) . " Payment Plan for " . htmlspecialchars($project['projectLocation']), ["size" => 14, 'bold' => true, 'color' => $this->primaryColor]);

            $paymentsTable = $section->addTable($tableStyle);
            $row           = $paymentsTable->addRow();

            $row->addCell(null, ['bgColor' => $this->primaryColor])->addText("#", ['color' => 'ffffff', 'align' => 'center'], array('align' => 'center'));

            $row->addCell(null, ['bgColor' => $this->primaryColor])->addText("Milestone", ['color' => 'ffffff'], array('align' => 'center'));

            $row->addCell(null, ['bgColor' => $this->primaryColor])->addText("%", ['color' => 'ffffff'], array('align' => 'center'));

            $row->addCell(null, ['bgColor' => $this->primaryColor])->addText("Date", ['color' => 'ffffff'], array('align' => 'center'));

            $row->addCell(null, ['bgColor' => $this->primaryColor])->addText("Amount (AED)", ['color' => 'ffffff'], array('align' => 'center'));

            $serial = 0;
            foreach ($paymentPlans['milestones'] as $milestoneIndex => $planRow) {
                $row    = $paymentsTable->addRow();
                $cStyle = $milestoneIndex % 2 ? ['bgColor' => 'eeeeee'] : [];

                $cell = $row->addCell(null, $cStyle);
                $cell->addText((++$serial), ['color' => $this->textColor], array('align' => 'center'));

                $planRow['amount'] = ($project[$this->form === 'first'? 'totalPrice': 'saleablePrice'] / 100) * $planRow['percentage'];
                foreach (array_keys($planRow) as $col) {
                    $text = $planRow[$col];
                    if ($col == 'amount' && is_numeric($planRow[$col])) {
                        $text = number_format($planRow[$col]);
                    }
                    if ($col == 'date') {
                        $text = $planRow['date'] ? date('M d, Y', strtotime($planRow['date'])) : 'N/A';
                    }
                    $cell = $row->addCell(null, $cStyle);
                    $cell->addText(htmlspecialchars($text), ['color' => $this->textColor], array('align' => 'center'));
                }
            }
            // dump($project);

            if (count($project['notes'])) {
                $phpWord->addNumberingStyle('multilevel', array('type' => 'multilevel', 'levels' => array(array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360), array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),)));
                $section->addText("");
                $section->addText("\n\rNotes:\n\r", ["size" => 14, 'bold' => true, 'color' => $this->primaryColor]);
                foreach ($project['notes'] as $note) {
                    $section->addListItem(htmlspecialchars($note['title']), 0, ['color' => $this->textColor]);
                }
            }

            if ($projectIndex < (count($this->offer['projects']) - 1)) $section->addPageBreak();
        }
    }

    private function setFooter($section)
    {
        $footer = $section->addFooter();

        $table_style = new \PhpOffice\PhpWord\Style\Table;
        /*$table_style->setBorderColor('cccccc');
        $table_style->setBorderSize(1);*/
        $table_style->setUnit(\PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT);
        $table_style->setWidth(100 * 50);

        $footerTable    = $footer->addTable($table_style);
        $footerRow      = $footerTable->addRow();
        $footerTextCell = $footerRow->addCell(null, ['valign' => 'bottom']);
        $footerTextCell->addText("Generated on " . date('d.m.Y'));
        $footerTextCell = $footerRow->addCell(1000);
        $footerTextCell->addImage(realpath(public_path("img/logo.png")), array('width' => 100, 'align' => 'right', 'marginTop' => -100, 'marginLeft' => -1, 'wrappingStyle' => 'behind'));
    }

    private function setTopImage($section)
    {
        $header = $section;

        $table_style = new \PhpOffice\PhpWord\Style\Table;
        /*$table_style->setBorderColor('cccccc');
        $table_style->setBorderSize(1);
        $table_style->setUnit(\PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT);*/
        $table_style->setUnit(TblWidth::PERCENT);
        $table_style->setWidth(100 * 50);

        $headerTable = $header->addTable($table_style);
        $headerRow   = $headerTable->addRow();

        $headerRow->addCell(1000)->addImage(realpath(public_path("img/logo.png")), array('width' => 100, 'align' => 'left', 'marginTop' => -1, 'marginLeft' => -1, 'wrappingStyle' => 'behind'));

        $headerRow->addCell(null, ['valign' => 'top'])->addText("Date: " . date('d-M-Y'), ['color' => $this->textColor], ['align' => 'right']);
    }

    private function displayProjectTableHeader($projectTable)
    {
        $cellTextStyle = ['color' => 'ffffff', 'width' => 150 * 50];
        $cellStyle     = ['width' => 100 * 50, 'bold' => true];

        $row = $projectTable->addRow();
        $row->addCell(null, $cellStyle)->addText("Project / Location", $cellTextStyle, array('align' => 'center'));

        if ($this->form === 'first') {
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("Plot #", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("Plot Size (Sq. Ft)", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("Built Up Area (Sq. Ft)", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("Price/Sq. Ft (AED)", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("Total Price (AED)", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("Plot Use", $cellTextStyle, array('align' => 'center'));
        }else{
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("Unit #", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("No of Bedrooms", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("No of Bathrooms", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(null, $cellStyle);
            $cell->addText("Saleable Area (SQ. FT)", $cellTextStyle, array('align' => 'center'));
            $cell = $row->addCell(55*50, $cellStyle);
            $cell->addText("Selling Price (AED)", $cellTextStyle, array('align' => 'center'));
        }
    }
}
