<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaleOffer2Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerName'                                          => 'required',
            'welcomeNote'                                           => 'required',
            'brokerName'                                            => 'required',
            'brokerDesignation'                                     => 'required',
//            'brokerRegistrationNumber'                              => 'required',
            'projects'                                              => 'required|array',
            'projects.*.unitNo'                                     => 'required',
            'projects.*.noBedrooms'                                 => 'required|numeric',
            'projects.*.noBathrooms'                                => 'required|numeric',
            'projects.*.saleableSquareFit'                          => 'required',
            'projects.*.saleablePrice'                              => 'required|numeric',
            'projects.*.projectLocation'                            => 'required',

            'projects.*.paymentPlan'                                => 'required',
            'projects.*.paymentPlan.noYears'                        => 'required',
            'projects.*.paymentPlan.milestones'                     => 'required|array',
            'projects.*.paymentPlan.milestones.*.milestone'         => 'required',
            'projects.*.paymentPlan.milestones.*.percentage'        => 'required|numeric',
            // 'projects.*.notes'          => 'required|array',
            // 'projects.*.paymentPlan.milestones.*.date'             => 'required',

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'projects.*.noYears.required'                                   => 'No of years is required',
            'projects.*.milestones.required'                                => 'At least one milestone is required',
            'projects.*.notes.required'                                     => 'At least one Note is required',

            'projects.*.unitNo.required'                                    => 'Unit no. is required',
            'projects.*.noBedrooms.required'                                => 'Number of Bedrooms is required',
            'projects.*.noBathrooms.required'                               => 'Number of Bathrooms is required',
            'projects.*.saleableSquareFit.required'                         => 'Saleable Square fit is required',
            'projects.*.saleablePrice.required'                             => 'Saleable Price is required',
            'projects.*.projectLocation.required'                           => 'Location is required',
            'projects.*.saleablePrice.numeric'                              => 'Saleable Price must be numeric value',

            'projects.*.paymentPlan.milestones.*.milestone.required'         => 'Milestone is required',
            'projects.*.paymentPlan.milestones.*.percentage.required'        => 'Percentage is required',
            // 'projects.*.paymentPlan.milestones.*.date.required'              => 'Date is required',
        ];
    }
}
