<?php

use Illuminate\Database\Seeder;

class NoteTableSeeder extends Seeder
{

    protected $notes;
    protected $notes2;

    function __construct() {

        $this->notes = [
            'Ownership - UAE & GCC National',
            'Ownership - Freehold',
            'Ownership - Non-GCC (Leasehold for 50 years)',
            'No time limit to construct',
            'Time limit to construct (as per developer)',
            'No service charge',
            'Service charge (as per developer)',
            '0% of Dubai Land Department is paid by the developer',
            '2% of Dubai Land Department is paid by the developer',
            '4% of Dubai Land Department is paid by the developer',
        ];

        $this->notes2 = [
            'AED 3,000 Oqood fee to be paid by buyer',
            '4% Dubai Land Department fee of the property net price paid by developer',
            '2% Dubai Land Department fee of the property net price paid by developer',
            'No time limit to construct',
            '4% Dubai Land Department fee of the property net price paid by buyer',
            '2% Dubai Land Department fee of the property net price paid by developer',
            'Attached is the location map &amp; floor plan',
        ];

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        foreach($this->notes as $note)
        {
            \App\Note::create(['title' => $note,  'sort' => ($i++), 'form' => 1]);
        }

        $i = 1;
        foreach($this->notes2 as $note)
        {
            \App\Note::create(['title' => $note,  'sort' => ($i++), 'form' => 2]);
        }
    }
}
