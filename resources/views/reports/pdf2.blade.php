<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Sales Offer</title>
    <style>
        *{
            color: #717271;
        }
        .primary-color {
            color: #0073ca;
        }
        table > thead > tr, table > thead > tr > th {
            background-color: #0073ca;
            color: white;
            font-size: 12px;
        }

        table > tbody > tr > td {
            font-size: 11px;
            text-align: center;
        }

        table > thead > tr > th {
            text-align: center;
        }

        h1.title {
            font-size: 28px;
            font-weight: bolder;
        }

        h3{
            font-size: 16px;
        }
        .page_break { page-break-before: always; }
        footer {
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            width: 100%;
        }
        @page { margin: 50px 25px 50px 25px; }
    </style>
</head>
<body>

{{-- Footer --}}
<footer class="container-fluid">
    <div class="float-left text-muted">
        <p style="font-size: 10px;" class="mt-3">Generated on {{ date('d.m.Y') }}</p>
    </div>
    <div class="float-right text-right">
        <img src="{{ realpath(public_path("img/logo.png") ) }}" alt="Logo" width="100">
    </div>
</footer>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <img src="{{ realpath(public_path("img/logo.png") ) }}" alt="Logo" id="logo" width="200" class="logo">
        </div>

        <div class="col-md-6">
            <p class="text-right">Date: {{ date('d-M-Y') }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center font-weight-bold primary-color title">SALES OFFER</h1>
            <p class="font-weight-bold">Dear {{ $offer['customerName'] }},</p>
            <p class="mb-4">{!! nl2br($offer['welcomeNote']) !!}</p>

            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Project / Location</th>
                    <th>Unit no.</th>
                    <th>No. Bedrooms</th>
                    <th>No.Bathrooms</th>
                    <th>Saleable Area (Sq. Ft)</th>
                    <th>Selling Price (AED)</th>
                </tr>
                </thead>
                <tbody>
                @foreach($offer['projects'] as $project)
                    <tr>
                        <td>{{ $project['projectLocation'] }}</td>
                        <td>{{ $project['unitNo'] }}</td>
                        <td>{{ number_format($project['noBedrooms']) }}</td>
                        <td>{{ number_format($project['noBathrooms']) }}</td>
                        <td>{{ number_format($project['saleableSquareFit']) }}</td>
                        <td>{{ number_format($project['saleablePrice']) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @foreach($offer['projects'] as $project)
                @php $paymentPlans = $project['paymentPlan']; @endphp
                <h3 class="font-weight-bold primary-color">{{ $paymentPlans['noYears'] }} years Payment Plan for {{ $project["projectLocation"] }}</h3>

                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Milestone</th>
                        <th>%</th>
                        <th>Date</th>
                        <th>Amount (AED)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paymentPlans['milestones'] as $milestoneIndex => $milestone)
                        <tr>
                            <td>{{ $milestoneIndex+1 }}</td>
                            <td>{{ $milestone['milestone'] }}</td>
                            <td>{{ $milestone['percentage'] }}</td>
                            <td>{{ $milestone['date']? date('M d, Y', strtotime($milestone['date'])) : 'N/A' }}</td>
                            <td>{{ number_format($project['saleablePrice'] / 100 * $milestone['percentage']) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if(!empty($project['notes']))
                    <h3 class="font-weight-bold primary-color mt-2">Notes:</h3>
                    <ul>
                    @foreach($project['notes'] as $note)
                         <li>{{ $note['title'] }}</li>
                    @endforeach
                    </ul>
                @endif

                @if(!$loop->last) <div class="page_break"></div> @endif

            @endforeach

            <p class="font-weight-bold mb-5" style="text-decoration: underline; font-style: italic;">*Please note that the availabilities are subject to change</p>

            <p class="font-weight-bold mb-4">Sincerely,</p>
            <p class="mb-0">{{ $offer['brokerName'] }}</p>
            <p class="mb-0">{{ $offer['brokerDesignation'] }}</p>
            {{--<p class="mb-0">{{ "BRN: " . $offer['brokerRegistrationNumber'] }}</p>--}}

        </div>
    </div>
</div>

</body>
</html>
