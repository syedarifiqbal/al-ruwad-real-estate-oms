const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
    .options({ processCssUrls: false });


/*if(process.env.NODE_ENV === 'test')
{
    process.env.MIX_BASE_URL = '/al-ruwad/public'
}
else{
    process.env.MIX_BASE_URL = ''
}*/

/*
module.exports = {
    NODE_ENV: '"production"',
    BASE_URL: '""',
}
*/
